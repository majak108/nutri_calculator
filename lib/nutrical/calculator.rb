# encoding: utf-8

module Nutrical
  class Calculator

    def self.nutritional_values(base, products, per_grams = 100.0)

      divide_cons = 100.0 / per_grams

      # Default values of nutritions.
      fats = 0.0
      saturated_fats = 0.0
      carbohydrates = 0.0
      sugars = 0.0
      proteins = 0.0
      fibre = 0.0
      salt = 0.0
      energy = 0.0
      products_weight = 0.0

      fats_p = 0.0
      saturated_fats_p = 0.0
      salt_p = 0.0
      carbohydrates_p = 0.0
      fibre_p = 0.0

      # Result hash with calculated nutrition values.
      result = Hash.new

      if (products && !products.empty?) || base

        products_weight = 0.0

        products.each do |p|
          p_d_100 = (p.weight / 100.0) / divide_cons

          fats += p_d_100 * p.amount * p.fats
          saturated_fats += p_d_100 * p.amount * p.saturated_fats
          carbohydrates += p_d_100 * p.amount * p.carbohydrates
          sugars += p_d_100 * p.amount * p.sugars
          proteins += p_d_100 * p.amount * p.proteins
          fibre += p_d_100 * p.amount * p.fibre
          salt += p_d_100 * p.amount * p.salt
          energy += p_d_100 * p.amount * p.energy

          products_weight += p.weight * p.amount
        end

        # Sets total_weight to all products weight.
        total_weight = products_weight

        # If base exists, add its nutrition values.
        if base && base.weight && base.weight > 0
          base_d_100 = ((base.weight - products_weight) / 100.0) / divide_cons

          # Base product is always as one peace, so no multiplication by amount is needed.
          fats += base_d_100 * base.fats
          saturated_fats += base_d_100 * base.saturated_fats
          carbohydrates += base_d_100 * base.carbohydrates
          sugars += base_d_100 * base.sugars
          proteins += base_d_100 * base.proteins
          fibre += base_d_100 * base.fibre
          salt += base_d_100 * base.salt
          energy += base_d_100 * base.energy

          # Only for mixes like musli or porridge, the total_weight should be equal to base weight.
          total_weight = base.weight
        end

        # This calculation must be before nutrition calculation, because that calculation rounds numbers
        # and distort daily percent calculations.

        fats = get_product_nutrition(fats, total_weight)
        saturated_fats = get_product_nutrition(saturated_fats, total_weight)
        carbohydrates = get_product_nutrition(carbohydrates, total_weight)
        sugars = get_product_nutrition(sugars, total_weight)
        proteins = get_product_nutrition(proteins, total_weight)
        fibre = get_product_nutrition(fibre, total_weight)
        salt = get_product_nutrition(salt, total_weight)
        energy = get_product_nutrition(energy, total_weight)

      end

      result[:fats] = fats
      result[:saturated_fats] = saturated_fats
      result[:carbohydrates] = carbohydrates
      result[:sugars] = sugars
      result[:proteins] = proteins
      result[:fibre] = fibre
      result[:salt] = salt
      result[:energy] = energy

      # returns result
      return result
    end

    def self.calculate_rdi(product)
      fats_p = 0.0
      saturated_fats_p = 0.0
      salt_p = 0.0
      carbohydrates_p = 0.0
      fibre_p = 0.0


      # Result as hash.
      {
        fats_p: fats_p,
        saturated_fats_p: saturated_fats_p,
        salt_p: salt_p,
        carbohydrates_p: carbohydrates_p,
        fibre_p: fibre_p,
      }
    end

    def self.daily_percent(nutrition, type, precision = 8)
      rdi = rdi_values
      rdi_value = rdi[type.to_sym]

      if nutrition && rdi_value
        ((nutrition / rdi_value) * 100).round(precision)
      else
        0.0
      end
    end

    private

    def self.get_product_nutrition(nutrition, total_weight, precision = 2)
      ((nutrition / total_weight) * 100).round(precision) if total_weight > 0
    end

    def self.rdi_values
      {
        calories: 2000,
        fats: 65,
        saturated_fats: 20,
        carbohydrates: 300,
        fibre: 25,
        energy: 0
      }
    end

  end
end